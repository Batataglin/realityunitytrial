using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardController : MonoBehaviour
{
    private const int MINIMUM_VALUE = 1;
    private const int MAXIMUM_VALUE = 11;

    [SerializeField] private Toggle m_Toggle;
    [SerializeField] private TextMeshProUGUI m_Text;

    public int Value { get; private set; }
    
    public bool IsSelected { get
        {
            return m_Toggle.isOn;
        } 
    }

    public void Initialize()
    {
        m_Text.enabled = false;
        m_Toggle.isOn = false;
        Value = Random.Range(MINIMUM_VALUE, MAXIMUM_VALUE);
        m_Text.text = Value.ToString();
    }

    public void ShowValue()
    {
        m_Text.enabled = true;
    }
}
