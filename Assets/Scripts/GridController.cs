using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;
using System.Collections;

public class GridController : MonoBehaviour
{
    private WaitForSeconds wait = new WaitForSeconds(1f);

    public UnityEvent<bool,int> OnShowSummary;

    private const int ROW_COUNT = 4;
    private const int TOP_SCORE = 21;

    [SerializeField] private CardController cardPrefab;
    [SerializeField] private GameObject rowPrefab;
    [SerializeField] private Transform gridRoot;

    private int matchScore;
    private readonly List<CardController> cardList = new List<CardController>();
    private Coroutine showCardValue;
    public List<CardController> CardList { get { return cardList; } }

    private void Awake()
    {
        Initialize();
    }

    public void Initialize()
    {
        for (int i = 0; i < ROW_COUNT; i++)
        {
            GameObject row = Instantiate(rowPrefab, gridRoot);

            for (int j = 0; j < ROW_COUNT; j++)
            {
                CardController card = Instantiate(cardPrefab, row.transform);
                card.Initialize();

                CardList.Add(card);
            }
        }
    }

    public void HandleGameRestart()
    {
        foreach (CardController card in CardList)
        {
            card.Initialize();
        }    
    }
    public void CheckBet()
    {
        int currentBetValue = 0;

        foreach (var card in CardList)
        {
            if(card.IsSelected)
            {
                currentBetValue += card.Value;
                card.ShowValue();
            }
        }

        if(currentBetValue > TOP_SCORE)
        {
            currentBetValue = Mathf.FloorToInt(currentBetValue / 2f);
        }

        matchScore = currentBetValue;

        if (showCardValue != null)
        {
            StopCoroutine(showCardValue);
            showCardValue = null;

            showCardValue = StartCoroutine(ShowCardsValue());
        }
        else
        {
            showCardValue = StartCoroutine(ShowCardsValue());
        }

    }
    
    private IEnumerator ShowCardsValue()
    {
        yield return wait;

        OnShowSummary.Invoke(matchScore >= 60, matchScore);
    }
}
