using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using static UISummaryScreen;

public class UIController : MonoBehaviour
{
    public UnityEvent OnClick;
    public UnityEvent OnGameRestart;

    [SerializeField] private Canvas gameScreen;
    [SerializeField] private UISummaryScreen summaryScreen;

    [SerializeField] private Button betButton;

    private void Awake()
    {
        summaryScreen.CloseSummary();
        betButton.onClick.AddListener(BetClick);
    }

    private void BetClick()
    {
        betButton.interactable = false;
        OnClick?.Invoke();
    }

    public void ToggleGameScreen(bool winner, int score)
    {
        ToggleScreen();

        summaryScreen.ShowSummary(new SummaryInfoDM() { PlayerWon = winner, Score = score }, OnClose);
    }

    private void OnClose()
    {
        betButton.interactable = true;
        OnGameRestart?.Invoke();
        ToggleScreen();
    }

    private void ToggleScreen()
    {
        gameScreen.enabled = !gameScreen.enabled;
    }
}
