using UnityEngine;

/// <summary>
/// This class has the purpose to comunicate between UI and GAME through events
/// </summary>
public class ControllerManager : MonoBehaviour
{
    [SerializeField] private UIController uiController;
    [SerializeField] private GridController gridController;

    private void Awake()
    {
        gridController.OnShowSummary.AddListener(uiController.ToggleGameScreen);

        uiController.OnClick.AddListener(gridController.CheckBet);
        uiController.OnGameRestart.AddListener(gridController.HandleGameRestart);
    }
}
